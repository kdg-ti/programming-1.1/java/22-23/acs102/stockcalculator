package m1;


import java.util.Scanner;

public class StockCalculator {
  public static void main(String[] args) {
    Scanner keyboard =  new Scanner(System.in);
    System.out.print("Enter the pruchase price of the stock: ");
    double purchasePrice=keyboard.nextDouble();
    System.out.print("Enter the money you had to spend: ");
    double moneyAvailable=keyboard.nextDouble();
    int sharesPurchased = (int) (moneyAvailable/purchasePrice);

    double moneySpent = purchasePrice * sharesPurchased;
    System.out.println("You bought "+ sharesPurchased  +" shares for " + moneySpent);
    System.out.print("Enter the current price of the stock: ");
    double currentPrice = keyboard.nextDouble();
    double changeOfCapital = currentPrice * sharesPurchased - moneySpent;
    if(changeOfCapital > 0){
      System.out.print("Your profit is ");
    } else {
      System.out.print("Your loss is ");
    }
    System.out.println( Math.abs(changeOfCapital));
  }
}
